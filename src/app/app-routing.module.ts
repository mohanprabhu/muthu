import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterCustomerComponent }  from './register-customer/register-customer.component';
import { RegisterOwnerDetailComponent } from './register-owner-detail/register-owner-detail.component';
import { ViewCustomerDetailComponent } from './view-customer-detail/view-customer-detail.component';
import { ViewOwnerDetailComponent } from './view-owner-detail/view-owner-detail.component';
 
const routes: Routes = [
  {path:'', redirectTo:'register/view', pathMatch:'full'},
  {path:'register/view', component: RegisterCustomerComponent},
  {path:'register/owner', component: RegisterOwnerDetailComponent },
  {path: 'view/customer', component: ViewCustomerDetailComponent},
  {path: 'view/owner', component: ViewOwnerDetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
