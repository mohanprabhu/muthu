import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';


import { AppComponent } from './app.component';
import { RegisterCustomerComponent } from './register-customer/register-customer.component';
import { RegisterOwnerDetailComponent } from './register-owner-detail/register-owner-detail.component';
import { ViewCustomerDetailComponent } from './view-customer-detail/view-customer-detail.component';
import { ViewOwnerDetailComponent } from './view-owner-detail/view-owner-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterCustomerComponent,
    RegisterOwnerDetailComponent,
    ViewCustomerDetailComponent,
    ViewOwnerDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
