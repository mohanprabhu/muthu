import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Dummy } from './Dummy-data';
import { Customer } from './model-customer';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  getCustomer():Observable<Customer[]>{
    return of(Dummy);

  }
  constructor() { }
}
