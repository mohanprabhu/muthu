import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { CustomerService } from '../customer.service';
import { Customer } from '../model-customer';

@Component({
  selector: 'app-register-customer',
  templateUrl: './register-customer.component.html',
  styleUrls: ['./register-customer.component.scss']
})

export class RegisterCustomerComponent implements OnInit {
  
  adData:Customer[];

  customerForm = new FormGroup({
    customerName : new FormControl(''),
    customerPhone: new FormControl(''),
    customerLocation: new FormControl(''),
    customerPrice: new FormControl('')
  });

  addName(){
    this.customerForm.patchValue({customerName:this.adData[0].customerName,customerPhone:"prabhu",customerLocation:"I am",customerPrice:"Hello view"})
  }

  constructor(private CustomerService:CustomerService) { }
  
  onSubmit(){
    console.log(this.customerForm.value.customerName);
    console.log(this.adData[0].customerName);
  }

  getCustomer():void{
    this.CustomerService.getCustomer().subscribe(data => this.adData = data.slice(0, 1))
  }

  ngOnInit() {
    this.getCustomer();  
    this.addName();
    
  }

}
