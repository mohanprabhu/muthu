import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormGroupDirective, FormControlName} from '@angular/forms';

@Component({
  selector: 'app-register-owner-detail',
  templateUrl: './register-owner-detail.component.html',
  styleUrls: ['./register-owner-detail.component.scss']
})
export class RegisterOwnerDetailComponent implements OnInit {
  
  ownerDetail = new FormGroup({
    ownerName : new FormControl(''),
    ownerPhone : new FormControl(''),
    ownerLocation : new FormControl(''),
    ownerPrice : new FormControl('')
  })

  constructor() { }

  ngOnInit() {
  }

}
