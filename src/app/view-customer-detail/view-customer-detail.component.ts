import { Component, OnInit } from '@angular/core';
import { Customer } from '../model-customer';
import{ CustomerService } from '../customer.service';

@Component({
  selector: 'app-view-customer-detail',
  templateUrl: './view-customer-detail.component.html',
  styleUrls: ['./view-customer-detail.component.scss']
})
export class ViewCustomerDetailComponent implements OnInit {

  data:Customer[];

  constructor( private CustomerService:CustomerService ) { }

  getCustomer():void{
    this.CustomerService.getCustomer().subscribe(data => this.data = data)
  }

  ngOnInit() {
    this.getCustomer();
  }

}
